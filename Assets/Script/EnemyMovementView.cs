﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementView : MonoBehaviour
{
    // Start is called before the first frame update
    
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMovementModel enemyMovementModel = GetComponent<EnemyMovementModel>();
        enemyMovementModel.rb.velocity = new Vector2(enemyMovementModel.speed, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        EnemyMovementModel enemyMovementModel = GetComponent<EnemyMovementModel>();

        if (collision.gameObject.CompareTag("Pared"))
        {
            enemyMovementModel.speed = enemyMovementModel.speed * (-1);
        }
    }
}
