﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot()
    {
        EnemyShootingModel enemyShootingModel = GameObject.Find("Enemy").GetComponent<EnemyShootingModel>();
        
        GameObject temporalBullet = Instantiate(enemyShootingModel.bulletslot);
        temporalBullet.transform.position = enemyShootingModel.bulletspawn.transform.position;
        enemyShootingModel.timer = 0;
    }
}
