﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovementModel playerMovementModel = GetComponent<PlayerMovementModel>();

        Vector2 velocidad = new Vector2(0, 0);

        if (Input.GetKey(KeyCode.RightArrow))
        {
            velocidad.x = 10;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            velocidad.x = -10;
        }
        playerMovementModel.rb.velocity = velocidad;
    }
}
