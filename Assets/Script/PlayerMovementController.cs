﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerMovementModel playerMovementModel = GetComponent<PlayerMovementModel>();
        playerMovementModel.rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
