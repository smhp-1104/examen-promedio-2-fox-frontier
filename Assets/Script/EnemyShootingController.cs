﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EnemyShootingModel enemyShootingModel = GetComponent<EnemyShootingModel>();
        EnemyShootingView enemyShootingView = GetComponent<EnemyShootingView>();

        enemyShootingModel.timer = enemyShootingModel.timer + Time.deltaTime;

        if (enemyShootingModel.timer >= 1.5)
        {
            enemyShootingView.Shoot();

        }
    }
}
