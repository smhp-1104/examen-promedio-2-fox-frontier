﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    // Start is called before the first frame update

    public Rigidbody2D rb;
    public float timer;
    public GameObject bulletslot;
    public GameObject bulletspawn;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer = timer + Time.deltaTime;
        if (timer >= 1.5)
        {
            GameObject temporalBullet = Instantiate(bulletslot);
            temporalBullet.transform.position = bulletspawn.transform.position;
            timer = 0;
        }
    }
}
