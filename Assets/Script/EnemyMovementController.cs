﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        EnemyMovementModel enemyMovementModel = GetComponent<EnemyMovementModel>();
        enemyMovementModel.rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
